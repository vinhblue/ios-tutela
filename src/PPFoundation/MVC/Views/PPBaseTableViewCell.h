//
//  PPBaseTableViewCell.h
//  PPFoundation
//
//  Created by Hieu Bui on 3/5/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPBaseTableViewCell : UITableViewCell

@end
