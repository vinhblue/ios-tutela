//
//  CDCell.h
//  PPFoundation
//
//  Created by Master on 9/10/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPBaseTableViewCell.h"

@interface CDCell : PPBaseTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIImageView *daupanh;
@property (weak, nonatomic) IBOutlet UIImageView *chuaupanh;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
