//
//  CauChuyenVC.m
//  PPFoundation
//
//  Created by Master on 9/6/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "CauChuyenVC.h"

@interface CauChuyenVC () <NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *info1;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UILabel *info2;
@property (weak, nonatomic) IBOutlet UIView *myView;

@property (strong, nonatomic) NSFetchedResultsController* fetchedResultsController;
@property (nonatomic) CauChuyen * cauchuyen;
@end

@implementation CauChuyenVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.navigationController.navigationBarHidden = NO;
//    UIImage *image = [UIImage imageNamed:@"cauchuyen.png"];
//    
//    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:image];
    self.navigationItem.title = @"Câu Chuyện";
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    [self getCauChuyen];
    NSError* error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        DLog(@"%@", error.localizedDescription);
        abort();
    }
    
    _cauchuyen = [_fetchedResultsController.fetchedObjects lastObject];
    
    _image.layer.cornerRadius = 5;
    _image.clipsToBounds = YES;
    
    _name.font = [PPFontsHelper UVFFontWithSize:25];
//    _info1.textColor = [UIColor colorWithRed:42 green:73 blue:6 alpha:0.1];
//    _info2.textColor = [UIColor colorWithRed:42 green:73 blue:6 alpha:0.1];
    
    [self fillData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) fillData{
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: $S(@"http://tutela.vn/asset/images/mobile/%@",_cauchuyen.image )]]];
    
    _name.text = _cauchuyen.name;
    _info1.text = _cauchuyen.info1;
    _image.image = myImage;
    _info2.text = _cauchuyen.info2;
   
    }

- (void)viewDidLayoutSubviews
{
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, _info2.frame.origin.y + _info2.frame.size.height + 20);

}

- (void) getCauChuyen{
    // Call api to login
    [self.utility showLoadingView];
    [self.apiClient getCauChuyen:[[self.dataManager currentLoogedUser].user_id intValue] andFaceBookID:[[self.dataManager currentLoogedUser].fb_id doubleValue] completion:^(PPResponseObject *responseObject) {
        int success = [[responseObject.responseBody objectForKey:@"success"] intValue];
        if (success == 1) {
            NSDictionary * dict = [responseObject.responseBody objectForKey:@"story"];
            _name.text = [dict objectForKey:@"name"];
            _info1.text = [dict objectForKey:@"info1"];
            _info2.text = [dict objectForKey:@"info2"];
            
            UIImage* myImage = [UIImage imageWithData:
                                [NSData dataWithContentsOfURL:
                                 [NSURL URLWithString: $S(@"http://tutela.vn/asset/images/mobile/%@",[dict objectForKey:@"images"])]]];
            
            _image.image = myImage;
            [self.dataManager saveCauChuyen:responseObject.responseBody];
  
        }else{
            NSLog(@"data %@", responseObject.responseBody);
            NSString *message = [responseObject.responseBody objectForKey:@"message"];
            if ([message isEqualToString:@""] || message == nil) {
                message = @"Vui lòng kiểm tra kết nối mạng và thử lại !";
            }
            [[PPUtility sharedUtility] showMessage:message withTitle:@"Chú ý"];
        }
        [self.utility hideLoadingView];
    }];
    
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController = [CauChuyen MR_fetchAllSortedBy:@"cc_id" ascending:YES withPredicate:nil groupBy:nil delegate:self];
    return _fetchedResultsController;
}
@end
