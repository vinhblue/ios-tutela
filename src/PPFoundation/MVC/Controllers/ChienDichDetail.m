//
//  ChienDichDetail.m
//  PPFoundation
//
//  Created by Master on 9/10/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "ChienDichDetail.h"
#import "NSString+HTML.h"
@interface ChienDichDetail ()
- (IBAction)touch_step1:(id)sender;
- (IBAction)touch_step2:(id)sender;
- (IBAction)touch_step3:(id)sender;
- (IBAction)touch_step4:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *img_banner;
@property (weak, nonatomic) IBOutlet UIImageView *img_step1;
@property (weak, nonatomic) IBOutlet UILabel *lb_step1;
@property (weak, nonatomic) IBOutlet UIImageView *img_step2;
@property (weak, nonatomic) IBOutlet UILabel *lb_step2;
@property (weak, nonatomic) IBOutlet UIImageView *img_step3;
@property (weak, nonatomic) IBOutlet UILabel *lb_step3;
@property (weak, nonatomic) IBOutlet UIWebView *wv2;

@property (weak, nonatomic) IBOutlet UIImageView *img_step4;
@property (weak, nonatomic) IBOutlet UIImageView *img_picture;

@property (weak, nonatomic) IBOutlet UIButton *btn_take;
@property (weak, nonatomic) IBOutlet UIButton *btn_upload;
@property (weak, nonatomic) IBOutlet UILabel *end;
- (IBAction)touch_take:(id)sender;
- (IBAction)touch_upload:(id)sender;
@property (nonatomic) UIImagePickerController *imagePickerController;
@property (nonatomic) UITextField *entercomment;

@property (nonatomic) double time;

@end

@implementation ChienDichDetail

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.title = @"Tham Gia";
    [super viewDidLoad];

    _time = [[NSDate date] timeIntervalSince1970];
    if (_time > [_cdr.date_end doubleValue]) {
        NSLog(@"aaaa");
        _img_step1.hidden = YES;
        _img_step2.hidden = YES;
        _img_step3.hidden = YES;
        _img_step4.hidden = YES;
        _lb_step1.hidden = YES;
        _lb_step2.hidden = YES;
        _lb_step3.hidden = YES;
        
        _img_picture.hidden = YES;
        _btn_take.hidden = YES;
        _btn_upload.hidden = YES;
        
        
        _end.hidden = NO;
        _end.text = @"Chiến dịch đã kết thúc, cảm ơn bạn đã tham gia";
        _end.font = [PPFontsHelper UTMFontWithSize:25];
        
        
        
    }else{
        _end.hidden = YES;
        _img_step1.hidden = NO;
        _img_step2.hidden = NO;
        _img_step3.hidden = NO;
        _img_step4.hidden = NO;
        _lb_step1.hidden = NO;
        _lb_step2.hidden = NO;
        _lb_step3.hidden = NO;
        
        _img_picture.hidden = NO;
        _btn_take.hidden = NO;
        _btn_upload.hidden = NO;
        NSLog(@"bbbb");
        
        
    }
    
    for (int i = 0; i < [_cdr.steps allObjects].count; i++) {
        Step * st = [[_cdr.steps allObjects] objectAtIndex:i];
        if ([st.name isEqualToString:@"know it"]) {
            NSString *str = [NSString stringWithFormat:@"%@ \n %@",st.left,st.right];
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            
            
            _lb_step1.text = [attrStr string];
//            _lb_step1.text = [str stringByConvertingHTMLToPlainText];
//            _lb_step1.attributedText = attrStr;
        }else if([st.name isEqualToString:@"plan it"]){
            NSString *str = [NSString stringWithFormat:@"%@ \n %@",st.left,st.right];
            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            
            
            _lb_step2.text = [attrStr string];
        }else if([st.name isEqualToString:@"do it"]){
            NSString *str = [NSString stringWithFormat:@"%@ \n %@",st.left,st.right];

            NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[str dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            
            
            
            _lb_step3.text = [attrStr string];
        }
        
    }
    
    _img_banner.layer.cornerRadius = 5;
    _img_banner.clipsToBounds = YES;
    
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: $S(@"http://tutela.vn/asset/images/mobile/%@",_cdr.img_regis)]]];
    if(myImage != nil){
        _img_banner.image = myImage;
    }else{
        _img_banner.image = [UIImage imageNamed:@"logoggPlay.png"];
    }

    
    _img_picture.layer.cornerRadius = 5;
    _img_picture.clipsToBounds = YES;
 
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[_cdr.date_end doubleValue]];
    NSLog(@"%@", date);
    NSDate *date2 = [NSDate dateWithTimeIntervalSince1970:[_cdr.date_create doubleValue]];
    NSLog(@"%@", date2);
    NSDate *date3 = [NSDate dateWithTimeIntervalSince1970:[_cdr.create_at doubleValue]];
    NSLog(@"%@", date3);
}

- (void)viewDidLayoutSubviews
{
    _scrollView.contentSize = CGSizeMake(320,_btn_take.frame.origin.y + _btn_take.frame.size.height + 20);
    if(IS_IPHONE_4){
        _scrollView.contentSize = CGSizeMake(320,_btn_take.frame.origin.y + _btn_take.frame.size.height + 120);
    }
    if (_time > [_cdr.date_end doubleValue]) {
        _scrollView.contentSize = CGSizeMake(320,_end.frame.origin.y + _end.frame.size.height + 20);
    }
    //_time > [_cdr.date_end doubleValue]
}

- (void) textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 111) {
        _entercomment.text = textField.text;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
}

- (void)showActionSheetChoosePhoto
{
    NSString *title = @"Chọn ảnh gửi đăng kí";
    NSString *takePhoto = @"Chụp ảnh";
    NSString *choosePhoto = @"Chọn trong thư mục";
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Huỷ" destructiveButtonTitle:nil otherButtonTitles:takePhoto, choosePhoto, nil ];
    [actionSheet showInView:self.view];
}

#pragma mark -- Action sheet delegate --
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    self.imagePickerController = picker;
    picker.delegate = self;
    picker.allowsEditing = YES;
    if ([buttonTitle isEqualToString:@"Chụp ảnh"]) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        [self presentViewController:picker animated:YES completion:NULL];
    } else if ([buttonTitle isEqualToString:@"Chọn trong thư mục"]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}



#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _img_picture.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (IBAction)touch_step1:(id)sender {
    if (_time < [_cdr.date_end doubleValue]) {
        _scrollView.contentOffset = CGPointMake(_img_step1.frame.origin.x, _img_step1.frame.origin.y);
    }
    
}

- (IBAction)touch_step2:(id)sender {
    if (_time < [_cdr.date_end doubleValue]) {
        _scrollView.contentOffset = CGPointMake(_img_step2.frame.origin.x, _img_step2.frame.origin.y);
    }
    
}

- (IBAction)touch_step3:(id)sender {
    if (_time < [_cdr.date_end doubleValue]) {
        _scrollView.contentOffset = CGPointMake(_img_step3.frame.origin.x, _img_step3.frame.origin.y);
    }
    
}

- (IBAction)touch_step4:(id)sender {
    if (_time < [_cdr.date_end doubleValue]) {
        _scrollView.contentOffset = CGPointMake(_img_step4.frame.origin.x, _img_step4.frame.origin.y);
    }
    
}
- (IBAction)touch_take:(id)sender {
    [self showActionSheetChoosePhoto];
}

- (IBAction)touch_upload:(id)sender {
    [self showDialogUp];

}

- (void) showDialogUp{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Chia sẻ ảnh" message:nil delegate:self cancelButtonTitle:@"Huỷ" otherButtonTitles:@"Đồng ý", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = @"Mô tả về bức ảnh";
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
            [self.utility showLoadingView];
        [self.apiClient uploadFile:[[self.dataManager currentLoogedUser].user_id intValue] andFaceBookID:[[self.dataManager currentLoogedUser].fb_id doubleValue] cd_id:[_cdr.dk_id intValue] picture:[_img_picture image] comment:[alertView textFieldAtIndex:0].text completion:^(PPResponseObject *responseObject) {
                NSLog(@"data %@", responseObject.responseBody);
                int success = [[responseObject.responseBody objectForKey:@"success"] intValue];
                if (success == 1) {
                    
                    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation([_img_picture image]) forKey:@"image"];
                    [self.utility showMessage:@"Chia sẻ ảnh thành công!" withTitle:nil];
                    
                }else{
        
                    NSString *message = [responseObject.responseBody objectForKey:@"message"];
                    if ([message isEqualToString:@""] || message == nil) {
                        message = @"Vui lòng kiểm tra kết nối mạng và thử lại !";
                    }
                    [[PPUtility sharedUtility] showMessage:message withTitle:@"Chú ý"];
                }
                [self.utility hideLoadingView];
            }];

    }
}
@end
