//
//  DangKiVC.m
//  PPFoundation
//
//  Created by Master on 9/12/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "DangKiVC.h"

@interface DangKiVC ()
@property (weak, nonatomic) IBOutlet UIImageView *img_banner;
@property (weak, nonatomic) IBOutlet UIButton *btn_dangki;

- (IBAction)touch_dangki:(id)sender;
@end

@implementation DangKiVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Đăng Kí Chiến Dịch";
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background3.png"]]];
    
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: $S(@"http://tutela.vn/asset/images/mobile/%@",_cd.cd_img_regis)]]];
    if(myImage != nil){
        _img_banner.image = myImage;
    }else{
        _img_banner.image = [UIImage imageNamed:@"img_regis.png"];
    }

}

- (void) viewWillAppear:(BOOL)animated{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dangkiChienDich{
    // Call api to login
    [self.utility showLoadingView];
    [self.apiClient dangkiChienDich:[[self.dataManager currentLoogedUser].user_id intValue] andFaceBookID:[[self.dataManager currentLoogedUser].fb_id doubleValue] idChienDich:[_cd.cd_id intValue] completion:^(PPResponseObject *responseObject) {
        int success = [[responseObject.responseBody objectForKey:@"success"] intValue];
        if (success == 1) {
            NSLog(@"data %@", responseObject.responseBody);
            
            
        }else{
            NSLog(@"data %@", responseObject.responseBody);
            
            NSString *message = [responseObject.responseBody objectForKey:@"message"];
            if ([message isEqualToString:@""] || message == nil) {
                message = @"Vui lòng kiểm tra kết nối mạng và thử lại !";
            }
            [[PPUtility sharedUtility] showMessage:message withTitle:@"Chú ý"];
        }
        [self.utility hideLoadingView];
    }];
    
}



- (IBAction)touch_dangki:(id)sender {
    [self dangkiChienDich];
}
@end
