//
//  ChienDichVC.m
//  PPFoundation
//
//  Created by Master on 9/6/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "ChienDichVC.h"
#define kDirectionHorizontal 1
#define kCellWidth 320
#define kCellHeight 504
#define kPadding 0
@interface ChienDichVC () <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController* fetchedResultsController;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) NSMutableArray *listCd;


@property (nonatomic) ChienDich * choosed;
@end

@implementation ChienDichVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _listCd = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Chiến Dịch";
    self.automaticallyAdjustsScrollViewInsets = NO;
    NSError* error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        DLog(@"%@", error.localizedDescription);
        abort();
    }
    [self getListChienDich];
    

}

- (void) viewWillLayoutSubviews{
    if(IS_IPHONE_4){
        self.collectionView.frame = CGRectMake(0, 64, 320, 416);
//        NSLog(@"%ld",(long)_collectionView.frame.size.height);
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.collectionView.pagingEnabled = YES;
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    
#if kDirectionHorizontal
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
#else
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
#endif
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _fetchedResultsController.fetchedObjects.count;
    return [_listCd count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"MyCollectionCell";
    MyCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (IS_IPHONE_4) {
        cell.frame = CGRectMake(0, 0, 320, 416);
        cell.image.frame = CGRectMake(0, 0, 320, 416);
    }
    NSLog(@"chien dich cell");
    ChienDich *cd = _fetchedResultsController.fetchedObjects[indexPath.row];
//    ChienDichModel *cd = [_listCd objectAtIndex:indexPath.row];
    
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: $S(@"http://tutela.vn/asset/images/mobile/%@",cd.cd_adv)]]];
    if(myImage != nil){
        cell.image.image = myImage;
    }else{
        cell.image.image = [UIImage imageNamed:@"img_regis.png"];
    }
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kCellWidth, kCellHeight);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2 * kPadding;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2 * kPadding;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
#if kDirectionHorizontal
    return UIEdgeInsetsMake(0, kPadding, 0, kPadding); // horizontal scrolling
#else
    return UIEdgeInsetsMake(kPadding, 0, kPadding, 0); // vertical scrolling
#endif
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _choosed = _fetchedResultsController.fetchedObjects[indexPath.row];
    [self performSegueWithIdentifier:@"gotodangki" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"gotodangki"]) {
        DangKiVC * vc = segue.destinationViewController;
        vc.cd = self.choosed;
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getListChienDich{
    // Call api to login
    [self.utility showLoadingView];
    [self.apiClient getListChienDich:[[self.dataManager currentLoogedUser].user_id intValue] andFaceBookID:[[self.dataManager currentLoogedUser].fb_id doubleValue] completion:^(PPResponseObject *responseObject) {
        int success = [[responseObject.responseBody objectForKey:@"success"] intValue];
        if (success == 1) {
            NSLog(@"data %@", responseObject.responseBody);
//            NSMutableArray * listRs = [responseObject.responseBody objectForKey:@"list_cd"];
//            for (int i =0; i < [listRs count]; i++) {
//                NSDictionary *dict = [listRs objectAtIndex:i];
//                ChienDichModel *cd = [[ChienDichModel alloc] init];
//                cd.cd_adv = [dict objectForKey:@"img_adv"];
//                cd.cd_id = [dict objectForKey:@"id_chiendich"];
//                cd.cd_idm = [dict objectForKey:@"id_cdm"];
//                cd.cd_img_list = [dict objectForKey:@"img_list"];
//                cd.cd_img_regis = [dict objectForKey:@"img_regis"];
//                
//                NSLog(@"%@,%@,%@",cd.cd_img_regis,cd.cd_adv,cd.cd_id);
//                [_listCd addObject:cd];
//                
//            }
//            [_collectionView reloadData];
            [self.dataManager saveChienDich:responseObject.responseBody];
  
        }else{
            NSLog(@"data %@", responseObject.responseBody);
            NSString *message = [responseObject.responseBody objectForKey:@"message"];
           
            if ([message isEqualToString:@""] || message == nil) {
                message = @"Vui lòng kiểm tra kết nối mạng và thử lại !";
            }
            [[PPUtility sharedUtility] showMessage:message withTitle:@"Chú ý"];
        }
        [self.utility hideLoadingView];
    }];

}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController = [ChienDich MR_fetchAllSortedBy:@"cd_idm" ascending:YES withPredicate:nil groupBy:nil delegate:self];
    return _fetchedResultsController;
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath{
    [_collectionView reloadData];
}


@end
