//
//  BaseViewController.h
//  PPFoundation
//
//  Created by Hieu Bui on 3/4/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPDataManager.h"
#import "PPAPIClient.h"
#import "PPSession.h"
#import "PPFBClient.h"

@interface PPBaseViewController : UIViewController

@property (strong, nonatomic, readonly) PPDataManager *dataManager;
@property (strong, nonatomic, readonly) PPAPIClient *apiClient;
@property (strong, nonatomic, readonly) PPSession *session;
@property (strong, nonatomic, readonly) PPFBClient *fbClient;
@property (strong, nonatomic, readonly) PPUtility *utility;


@end
