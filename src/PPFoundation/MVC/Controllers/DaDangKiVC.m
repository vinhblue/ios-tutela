//
//  DaDangKiVC.m
//  PPFoundation
//
//  Created by Master on 9/6/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "DaDangKiVC.h"

@interface DaDangKiVC () <NSFetchedResultsControllerDelegate>

@property (nonatomic) ChienDichRed * choosed;
@property (strong, nonatomic) NSFetchedResultsController* fetchedResultsController;
@end

@implementation DaDangKiVC

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Đã Đăng Kí";
    
    // Active nsfetchedresultscontroller
    NSError* error = nil;
    if (![[self fetchedResultsController] performFetch:&error]) {
        DLog(@"Unresolve error:%@",error.userInfo);
    }

    
    
    [self getListChienDich];
    [self.tableView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) getListChienDich{
//    // Call api to login
    [[PPUtility sharedUtility] showLoadingView];
    [[PPAPIClient sharedClient] getListChienDichDaDangKi:[[[PPDataManager sharedManager] currentLoogedUser].user_id intValue] andFaceBookID:[[[PPDataManager sharedManager] currentLoogedUser].fb_id doubleValue] completion:^(PPResponseObject *responseObject) {
        NSLog(@"data %@", responseObject.responseBody);
        int success = [[responseObject.responseBody objectForKey:@"success"] intValue];
        if (success == 1) {
            [[PPDataManager sharedManager] saveChienDichRed:responseObject.responseBody];
        }else{
            NSLog(@"data %@", responseObject.responseBody);
            NSString *message = [responseObject.responseBody objectForKey:@"message"];
            
            if ([message isEqualToString:@""] || message == nil) {
                message = @"Vui lòng kiểm tra kết nối mạng và thử lại !";
            }
            [[PPUtility sharedUtility] showMessage:message withTitle:@"Chú ý"];        }
        [[PPUtility sharedUtility] hideLoadingView];
    }];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _fetchedResultsController.fetchedObjects.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * cellIndentifier = @"cellCD";
    CDCell *cdCell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier forIndexPath:indexPath];
    if (cdCell == nil) {
        cdCell = (CDCell *)[PPUtility getCell:[CDCell class] owner:self];
    }
//    cdCell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"background.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    cdCell.backgroundColor = [UIColor clearColor];
    cdCell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"background3.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
    
    UIColor *color = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    [self.tableView setSeparatorColor:color];
    
    ChienDichRed *cdr = _fetchedResultsController.fetchedObjects[indexPath.row];
    
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: $S(@"http://tutela.vn/asset/images/mobile/%@",cdr.img_list )]]];
    NSLog(@"adv: %@",cdr.banner);
    cdCell.title.text = cdr.title;
    cdCell.title.textColor = [UIColor whiteColor];
    cdCell.title.font = [PPFontsHelper UTMFontWithSize:20];
    
    cdCell.avatar.clipsToBounds = YES;
    cdCell.avatar.layer.cornerRadius = 30;
    
    if([cdr.stt intValue] == 1){
        cdCell.chuaupanh.hidden = NO;
        cdCell.daupanh.hidden = YES;
        if(myImage != nil){
            cdCell.avatar.image = myImage;
        }else{
            cdCell.avatar.image = [UIImage imageNamed:@"image_cd.png"];
        }
    }else{
        NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"image"];
        UIImage* image = [UIImage imageWithData:imageData];
        if(image != nil){
            cdCell.avatar.image = image;
        }else{
            cdCell.avatar.image = [UIImage imageNamed:@"image_cd.png"];
        }
        cdCell.chuaupanh.hidden = YES;
        cdCell.daupanh.hidden = NO;
    }
    return cdCell;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    _choosed = _fetchedResultsController.fetchedObjects[indexPath.row];
    [self performSegueWithIdentifier:@"gotodetail" sender:self];
    
    NSLog(@"loggggggg %@", _choosed.title);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"gotodetail"]) {
        ChienDichDetail* vc = segue.destinationViewController;
        vc.cdr = self.choosed;

    }
}

#pragma mark - NSFetchedResultsControllerDelegate
- (NSFetchedResultsController *)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    _fetchedResultsController = [ChienDichRed MR_fetchAllSortedBy:@"dk_id" ascending:YES withPredicate:nil groupBy:nil delegate:self];
//    [self.tableView reloadData];
    return _fetchedResultsController;
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    [self.tableView reloadData];
//    UITableView *tableView = self.tableView;
//    
//    switch(type) {
//            
//        case NSFetchedResultsChangeInsert:
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//            
//        case NSFetchedResultsChangeDelete:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
//            break;
//            
//        case NSFetchedResultsChangeUpdate:{
//            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//        }
//            
//            break;
//            
//        case NSFetchedResultsChangeMove:
//            [tableView deleteRowsAtIndexPaths:[NSArray
//                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
//            [tableView insertRowsAtIndexPaths:[NSArray
//                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationNone];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

@end
