//
//  HomeVC.m
//  PPFoundation
//
//  Created by Master on 9/6/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "HomeVC.h"

@interface HomeVC ()

@property (weak, nonatomic) IBOutlet UIImageView *image_logo;
@property (weak, nonatomic) IBOutlet UIButton *btn_cauchuyen;
@property (weak, nonatomic) IBOutlet UIButton *btn_dangki;
@property (weak, nonatomic) IBOutlet UIButton *btn_chiendich;

- (IBAction)touch_cauchuyen:(id)sender;
- (IBAction)touch_dadangki:(id)sender;
- (IBAction)touch_chiendich:(id)sender;


@end

@implementation HomeVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_main_no.png"]]];

    
    
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    self.navigationItem.title = @"Home";
    
	// Hidden navibar
	[self.navigationController setNavigationBarHidden:YES animated:YES];

}



- (IBAction)touch_cauchuyen:(id)sender {

    [self performSegueWithIdentifier:@"gotocauchuyen" sender:self];
}

- (IBAction)touch_dadangki:(id)sender {
    [self performSegueWithIdentifier:@"gotodadangki" sender:self];
}

- (IBAction)touch_chiendich:(id)sender {
    [self performSegueWithIdentifier:@"gotochiendich" sender:self];
}
@end
