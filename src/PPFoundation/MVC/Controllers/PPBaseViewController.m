//
//  BaseViewController.m
//  PPFoundation
//
//  Created by Hieu Bui on 3/4/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import "PPBaseViewController.h"

@interface PPBaseViewController ()

@property (nonatomic, strong) PPDataManager *dataManager;
@property (nonatomic, strong) PPAPIClient *apiClient;
@property (nonatomic, strong) PPSession *session;
@property (nonatomic, strong) PPFBClient *fbClient;
@property (strong, nonatomic) PPUtility *utility;

@end

@implementation PPBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
   #pragma mark - Navigation

   // In a storyboard-based application, you will often want to do a little preparation before navigation
   - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
   {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
   }
 */



- (PPDataManager *)dataManager {
	if (nil == _dataManager) {
		_dataManager = [PPDataManager sharedManager];
	}

	return _dataManager;
}

- (PPAPIClient *)apiClient {
	if (nil == _apiClient) {
		_apiClient = [PPAPIClient sharedClient];
	}
	return _apiClient;
}

- (PPSession *)session {
	if (nil == _session) {
		_session = [PPSession sharedSession];
	}
	return _session;
}

- (PPFBClient *)fbClient{
    if (nil == _fbClient) {
        _fbClient = [PPFBClient fbClient];
    }
    return _fbClient;
}

- (PPUtility *)utility{
    if (nil == _utility) {
        _utility = [PPUtility sharedUtility];
    }
    return _utility;
}




@end
