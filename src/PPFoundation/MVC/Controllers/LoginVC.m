//
//  LoginVC.m
//  PPFoundation
//
//  Created by Master on 9/23/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "LoginVC.h"

@interface LoginVC ()
@property (weak, nonatomic) IBOutlet UITextField *txt_email;
@property (weak, nonatomic) IBOutlet UITextField *txt_address;
@property (weak, nonatomic) IBOutlet UITextField *txt_birth;
- (IBAction)touch_login:(id)sender;

@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (![PPSession sharedSession].isAuthenticated) {
        [self LoginWithFaceBook];
    }
    
    _txt_address.delegate = self;
    _txt_birth.delegate = self;
    _txt_birth.keyboardType = UIKeyboardTypeNumberPad;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
	[self.navigationController setNavigationBarHidden:YES animated:YES];
    
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField{
    if (IS_IPHONE_4) {
    
    if (textField == _txt_address) {
        [self.utility animateTextField:textField up:YES distance:50 view:self.view];
    } else if(textField == _txt_birth){
        [self.utility animateTextField:textField up:YES distance:150 view:self.view];
    }
    }
    return YES;
}

- (BOOL) textFieldShouldEndEditing:(UITextField *)textField{
    if(IS_IPHONE_4){
    if (textField == _txt_address) {
        [self.utility animateTextField:textField up:NO distance:50 view:self.view];
    } else if(textField == _txt_birth){
        [self.utility animateTextField:textField up:NO distance:150 view:self.view];
    }
    }
    return YES;
}


- (void) LoginWithFaceBook{
    // Show loading
    [self.utility showLoadingView];
    
    // Logout first
    [self.fbClient logout];
    
    // Retrieve facebook account credentials
    [self.fbClient loginCompletion:^(NSString *fbToken, NSError* error) {
        if (!error) {
            [self.fbClient getMeProfileCompletion:^(NSError* errorPrf, NSDictionary <FBGraphUser> *user) {
                if (!errorPrf) {
                    NSUserDefaults *defauld = [NSUserDefaults standardUserDefaults];
                    [defauld setObject:[PPValidator getSafeString:user[@"id"]] forKey:@"fb_id"];
                    [defauld setObject:[PPValidator getSafeString:user[@"email"]] forKey:@"email"];
                    [defauld setObject:[PPValidator getSafeString:user[@"first_name"]] forKey:@"fn"];
                    [defauld setObject:[PPValidator getSafeString:user[@"last_name"]] forKey:@"ln"];
                    _txt_email.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
                }else{
                    [self.utility showMessage:errorPrf.description withTitle:nil];
                }
                [self.utility hideLoadingView];
            }];
        }else{
            [self.utility showMessage:error.description withTitle:nil];
            [self.utility hideLoadingView];
        }
    }];
    
}



- (IBAction)touch_login:(id)sender {
    if ([self checkValidate]) {
        [self.utility showLoadingView];
        [self.apiClient loginBySocial:[[NSUserDefaults standardUserDefaults] objectForKey:@"fb_id"]
                                email:_txt_email.text
                            firstName:[[NSUserDefaults standardUserDefaults] objectForKey:@"fn"]
                             lastName:[[NSUserDefaults standardUserDefaults] objectForKey:@"ln"]
                              address:_txt_address.text
                                birth:[_txt_birth.text intValue]
                           completion:^(PPResponseObject *responseObject) {
                               
                               NSLog(@"data %@", responseObject.responseBody);
                               if (responseObject.statusCode == PPStatusCodeSuccess) {
                                   NSDictionary * user = [responseObject.responseBody objectForKey:@"user"];
                                   User * storeUser = [User MR_createEntity];
                                   storeUser.user_id = [NSNumber numberWithInt:[[user objectForKey:@"user_id"] intValue]];
                                   storeUser.fb_id = [user objectForKey:@"fb_id"];
                                   storeUser.create_at =
                                   [NSNumber numberWithInt:[[user objectForKey:@"date_reg"] intValue]];
                                   [storeUser.managedObjectContext MR_saveOnlySelfAndWait];
                                   PPSession* session = [PPSession sharedSession];
                                   session.isAuthenticated = YES;
                                   session.currentUserId = storeUser.user_id;
                                   [session save];
                                   [self performSegueWithIdentifier:@"loginsuccess" sender:self];
                               }else{
                                   
                                   // when error occurred
                                   [self.utility showMessage:@"Error !" withTitle:nil];
                               }
                               
                               [self.utility hideLoadingView];
                           }];

    }
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


- (BOOL) checkValidate{
    if (![self NSStringIsValidEmail:_txt_email.text] || StringIsNullOrEmpty(_txt_address.text) || StringIsNullOrEmpty(_txt_birth.text)) {
        [self.utility showMessage:@"Kiểm tra lại email, địa chỉ hoặc năm sinh" withTitle:nil];
        return NO;
        
    }
    return YES;
}
@end
