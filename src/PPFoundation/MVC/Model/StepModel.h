//
//  StepModel.h
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPBaseModel.h"

@interface StepModel : PPBaseModel
@property (nonatomic, retain) NSNumber <Optional>* cdr_id;
@property (nonatomic, retain) NSString <Optional>* left;
@property (nonatomic, retain) NSString <Optional>* right;
@property (nonatomic, retain) NSString <Optional>* name;
@end
