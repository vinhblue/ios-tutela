//
//  ChienDichModel.m
//  PPFoundation
//
//  Created by Master on 9/15/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "ChienDichModel.h"

@implementation ChienDichModel

/**
 *  @property (nonatomic, retain) NSString <Optional>* cd_adv;
 @property (nonatomic, retain) NSNumber <Optional>* cd_id;
 @property (nonatomic, retain) NSNumber <Optional>* cd_idm;
 @property (nonatomic, retain) NSString <Optional>* cd_img_regis;
 @property (nonatomic, retain) NSString <Optional>* cd_img_list;
 *
 *  @return <#return value description#>
 */

+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id_chiendich": @"cd_id",
                                                       @"id_cdm":@"cd_idm",
                                                       @"img_adv":@"cd_adv",
                                                       @"img_regis":@"cd_img_regis",
                                                       @"img_list":@"cd_img_list"}];
}

- (id) managerdObject{
    ChienDich* cd = [ChienDich MR_findFirstByAttribute:@"cd_idm" withValue:self.cd_idm];
    if (cd == nil) {
        cd = [ChienDich MR_createEntity];
        cd.cd_idm  = self.cd_idm;
        cd.cd_id = self.cd_id;
        cd.cd_adv = self.cd_adv;
        cd.cd_img_regis = self.cd_img_regis;
        cd.cd_img_list = self.cd_img_list;
        
        [cd.managedObjectContext MR_saveOnlySelfAndWait];
        return cd;
    }
    return cd;
}

@end
