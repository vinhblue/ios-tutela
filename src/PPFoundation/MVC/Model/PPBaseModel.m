//
//  PPBaseModel.m
//  PPFoundation
//
//  Created by Hieu Bui on 3/6/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import "PPBaseModel.h"

@implementation PPBaseModel

- (void)printDescription {
	DLog(@"%@", [self toJSONString]);
}

@end
