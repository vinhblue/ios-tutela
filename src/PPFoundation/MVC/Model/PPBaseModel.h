//
//  PPBaseModel.h
//  PPFoundation
//
//  Created by Hieu Bui on 3/6/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import "JSONModel.h"

@interface PPBaseModel : JSONModel

- (void)printDescription;
- (id) managerdObject;
@end
