//
//  ListCdModel.m
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "ListCdModel.h"

@implementation ListCdModel
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"list_cd": @"items"}];
}
@end
