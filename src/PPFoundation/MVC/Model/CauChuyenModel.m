//
//  CauChuyenModel.m
//  PPFoundation
//
//  Created by Master on 9/15/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "CauChuyenModel.h"

@implementation CauChuyenModel
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"story.id_story": @"cc_id",
                                                       @"story.images":@"image",
                                                       @"story.info1":@"info1",
                                                       @"story.info2":@"info2",
                                                       @"story.name":@"name"}];
}

- (id) managerdObject{
    CauChuyen* cc = [CauChuyen MR_findFirstByAttribute:@"cc_id" withValue:self.cc_id];
    if (cc == nil) {
        cc = [CauChuyen MR_createEntity];
        cc.cc_id  = self.cc_id;
        cc.image = self.image;
        cc.info1 = self.info1;
        cc.info2 = self.info2;
        cc.name = self.name;
        
        [cc.managedObjectContext MR_saveOnlySelfAndWait];
        return cc;
    }
    return cc;
}

@end
