//
//  ChienDich.h
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ChienDich : NSManagedObject

@property (nonatomic, retain) NSString * cd_adv;
@property (nonatomic, retain) NSNumber * cd_id;
@property (nonatomic, retain) NSNumber * cd_idm;
@property (nonatomic, retain) NSString * cd_img_list;
@property (nonatomic, retain) NSString * cd_img_regis;

@end
