//
//  CauChuyen.h
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CauChuyen : NSManagedObject

@property (nonatomic, retain) NSNumber * cc_id;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * info1;
@property (nonatomic, retain) NSString * info2;
@property (nonatomic, retain) NSString * name;

@end
