//
//  ListCDRModel.m
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "ListCDRModel.h"

@implementation ListCDRModel
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"list_dk": @"items"}];
}
@end
