//
//  ChienDichRedModel.m
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "ChienDichRedModel.h"

@implementation ChienDichRedModel



+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id": @"cd_id",
                                                       @"stt":@"stt",
                                                       @"chiendich.id":@"dk_id",
                                                       @"chiendich.create":@"create_at",
                                                       @"chiendich.date_create":@"date_create",
                                                       @"chiendich.date_end":@"date_end",
                                                       @"chiendich.sort_des":@"sort_des",
                                                       @"chiendich.title":@"title",
                                                       @"chiendich.image":@"image",
                                                       @"chiendich.banner":@"banner",
                                                       @"chiendich.step":@"items",
                                                       @"chiendich.cd_mobile.img_regis":@"img_regis",
                                                       @"chiendich.cd_mobile.img_list":@"img_list"
                                                       }];
}

- (id) managerdObject{
    ChienDichRed* cdr = [ChienDichRed MR_findFirstByAttribute:@"dk_id" withValue:self.dk_id];
    if (cdr == nil) {
        cdr = [ChienDichRed MR_createEntity];
    }
        cdr.dk_id  = self.dk_id;
        cdr.cd_id = self.cd_id;
        cdr.stt = self.stt;
        cdr.image = self.image;
        cdr.title = self.title;
        cdr.banner = self.banner;
        cdr.create_at = self.create_at;
        cdr.date_create = self.date_create;
        cdr.date_end = self.date_end;
        cdr.sort_des = self.sort_des;
    cdr.img_regis = self.img_regis;
    cdr.img_list = self.img_list;
    
        [self.items enumerateObjectsUsingBlock:^(StepModel* stM, NSUInteger idx, BOOL *stop) {
            [cdr addStepsObject:[stM managerdObject]];
        }];
    
        [cdr.managedObjectContext MR_saveOnlySelfAndWait];
        return cdr;
}

 @end
