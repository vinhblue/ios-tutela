//
//  ListCdModel.h
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPBaseModel.h"
#import "ChienDichModel.h"
@protocol ChienDichModel;

@interface ListCdModel : PPBaseModel
@property (strong, nonatomic) NSArray <ChienDichModel> * items;
@end
