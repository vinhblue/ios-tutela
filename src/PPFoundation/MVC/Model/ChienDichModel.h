//
//  ChienDichModel.h
//  PPFoundation
//
//  Created by Master on 9/15/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPBaseModel.h"

@interface ChienDichModel : PPBaseModel
@property (nonatomic, retain) NSString <Optional>* cd_adv;
@property (nonatomic, retain) NSNumber <Optional>* cd_id;
@property (nonatomic, retain) NSNumber <Optional>* cd_idm;
@property (nonatomic, retain) NSString <Optional>* cd_img_regis;
@property (nonatomic, retain) NSString <Optional>* cd_img_list;

@end
