//
//  ListCDRModel.h
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPBaseModel.h"
#import "ChienDichRedModel.h"
@protocol ChienDichRedModel;

@interface ListCDRModel : PPBaseModel
@property (strong, nonatomic) NSArray <ChienDichRedModel> * items;
@end
