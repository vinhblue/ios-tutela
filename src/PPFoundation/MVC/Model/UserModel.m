//
//  UserModel.m
//  PPFoundation
//
//  Created by Master on 9/10/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"user_id": @"user_id",
                                                       @"fb_id":@"fb_id",
                                                       @"date_reg":@"create_at"}];
}

- (id) managerdObject{
    User* user = [User MR_findFirstByAttribute:@"user_id" withValue:self.user_id];
    if (user == nil) {
        user = [User MR_createEntity];
        user.user_id  = self.user_id;
        user.fb_id = self.fb_id;
        user.create_at = self.create_at;
        
        [user.managedObjectContext MR_saveOnlySelfAndWait];
        return user;
    }
    return user;
}

@end
