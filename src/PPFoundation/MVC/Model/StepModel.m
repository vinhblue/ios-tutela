//
//  StepModel.m
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "StepModel.h"

@implementation StepModel
+ (JSONKeyMapper *)keyMapper{
    return [[JSONKeyMapper alloc] initWithDictionary:@{@"id_step": @"cdr_id",
                                                       @"left":@"left",
                                                       @"right":@"right",
                                                       @"step_name":@"name",
                                                       }];
}

- (id) managerdObject{
    Step* st = [Step MR_findFirstByAttribute:@"cdr_id" withValue:self.cdr_id];
    if (st == nil) {
        st = [Step MR_createEntity];
        st.name = self.name;
        st.cdr_id = self.cdr_id;
        st.left = self.left;
        st.right = self.right;
        
        [st.managedObjectContext MR_saveOnlySelfAndWait];
        return st;
    }
    return st;
}

@end
