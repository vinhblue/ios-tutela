//
//  UserModel.h
//  PPFoundation
//
//  Created by Master on 9/10/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPBaseModel.h"

@interface UserModel : PPBaseModel
@property (nonatomic, retain) NSNumber <Optional> * create_at;
@property (nonatomic, retain) NSString <Optional> * fb_id;
@property (nonatomic, retain) NSNumber <Optional> * user_id;
@end
