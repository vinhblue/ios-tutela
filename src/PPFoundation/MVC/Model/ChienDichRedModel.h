//
//  ChienDichRedModel.h
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPBaseModel.h"
#import "StepModel.h"
@protocol StepModel;

@interface ChienDichRedModel : PPBaseModel
@property (nonatomic, retain) NSString <Optional>* banner;
@property (nonatomic, retain) NSNumber <Optional>* create_at;
@property (nonatomic, retain) NSNumber <Optional>* date_create;
@property (nonatomic, retain) NSNumber <Optional>* date_end;
@property (nonatomic, retain) NSNumber <Optional>* dk_id;
@property (nonatomic, retain) NSString <Optional>* image;
@property (nonatomic, retain) NSString <Optional>* sort_des;
@property (nonatomic, retain) NSNumber <Optional>* stt;
@property (nonatomic, retain) NSString <Optional>* title;
@property (nonatomic, retain) NSNumber <Optional>* cdr_id;
@property (nonatomic, retain) NSNumber <Optional>* cd_id;
@property (nonatomic, retain) NSString <Optional>* img_regis;
@property (nonatomic, retain) NSString <Optional>* img_list;

@property (nonatomic, retain) NSArray <StepModel> * items;
@end
