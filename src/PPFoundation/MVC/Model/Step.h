//
//  Step.h
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChienDichRed;

@interface Step : NSManagedObject

@property (nonatomic, retain) NSNumber * cdr_id;
@property (nonatomic, retain) NSString * left;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * right;
@property (nonatomic, retain) ChienDichRed *cdr;

@end
