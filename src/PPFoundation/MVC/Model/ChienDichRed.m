//
//  ChienDichRed.m
//  PPFoundation
//
//  Created by Master on 9/23/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "ChienDichRed.h"
#import "Step.h"
#import "User.h"


@implementation ChienDichRed

@dynamic banner;
@dynamic cd_id;
@dynamic cdr_id;
@dynamic create_at;
@dynamic date_create;
@dynamic date_end;
@dynamic dk_id;
@dynamic image;
@dynamic sort_des;
@dynamic stt;
@dynamic title;
@dynamic img_regis;
@dynamic img_list;
@dynamic steps;
@dynamic user;


@end
