//
//  User.h
//  PPFoundation
//
//  Created by Master on 9/16/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ChienDichRed;

@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * create_at;
@property (nonatomic, retain) NSString * fb_id;
@property (nonatomic, retain) NSNumber * user_id;
@property (nonatomic, retain) NSSet *cd_red;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addCd_redObject:(ChienDichRed *)value;
- (void)removeCd_redObject:(ChienDichRed *)value;
- (void)addCd_red:(NSSet *)values;
- (void)removeCd_red:(NSSet *)values;

@end
