//
//  CauChuyenModel.h
//  PPFoundation
//
//  Created by Master on 9/15/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPBaseModel.h"

@interface CauChuyenModel : PPBaseModel
@property (nonatomic, retain) NSNumber <Optional> * cc_id;
@property (nonatomic, retain) NSString <Optional> * image;
@property (nonatomic, retain) NSString <Optional> * info1;
@property (nonatomic, retain) NSString <Optional> * info2;
@property (nonatomic, retain) NSString <Optional> * name;
@end
