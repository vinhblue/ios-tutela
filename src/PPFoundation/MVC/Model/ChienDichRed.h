//
//  ChienDichRed.h
//  PPFoundation
//
//  Created by Master on 9/23/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Step, User;

@interface ChienDichRed : NSManagedObject

@property (nonatomic, retain) NSString * banner;
@property (nonatomic, retain) NSNumber * cd_id;
@property (nonatomic, retain) NSNumber * cdr_id;
@property (nonatomic, retain) NSNumber * create_at;
@property (nonatomic, retain) NSNumber * date_create;
@property (nonatomic, retain) NSNumber * date_end;
@property (nonatomic, retain) NSNumber * dk_id;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * sort_des;
@property (nonatomic, retain) NSNumber * stt;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * img_regis;
@property (nonatomic, retain) NSString * img_list;

@property (nonatomic, retain) NSSet *steps;
@property (nonatomic, retain) User *user;
@end

@interface ChienDichRed (CoreDataGeneratedAccessors)

- (void)addStepsObject:(Step *)value;
- (void)removeStepsObject:(Step *)value;
- (void)addSteps:(NSSet *)values;
- (void)removeSteps:(NSSet *)values;

@end
