//
//  PPConstants.m
//  PPFoundation
//
//  Created by Hieu Bui on 3/5/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import "PPConstants.h"

NSString *const kAPIBaseURLString                           = @"http://api.tutela.vn/site/";
NSString *const kAPIBaseURLImageString                      = @"http://tutela.vn/asset/images/mobile";