//
//  PPConstants.h
//  PPFoundation
//
//  Created by Hieu Bui on 3/5/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark Enums
typedef enum {
	PPStatusCodeUndefined               = -1,
	PPStatusCodeSuccess                 = 200,
	PPStatusCodeCreated                 = 201,
	PPStatusCodeAccepted                = 202,
	PPStatusCodeBadRequest              = 400,
	PPStatusCodeAuthenticationFailure   = 401,
	PPStatusCodeForbidden               = 403,
	PPStatusCodeResourceNotFound        = 404,
	PPStatusCodeMethodNotAllowed        = 405,
	PPStatusCodeConflict                = 409,
	PPStatusCodePreconditionFailed      = 412,
	PPStatusCodeRequestEntityTooLarge   = 413,
	PPStatusCodeInternalServerError     = 500,
	PPStatusCodeNotImplemented          = 501,
	PPStatusCodeServiceUnavailable      = 505,
} PPStatusCode;

#pragma mark Strings

// iPad xib file postfix
static NSString *const kiPadXibPostFix = @"~iPad";

// Calling web service
extern NSString *const kAPIBaseURLString;
static NSString *const kGGPClientId = @"723495860317-guv9jbvsopa8ln1l4lb8uubusulal6fu.apps.googleusercontent.com";
static NSString *const kGGPClientSecret = @"h7YDA7PpqBew-kggnp9EKCAR";
static NSString *const kGTLAuthScopePlusLogin = @"https://www.googleapis.com/auth/plus.me";
static NSString* const kHttpMethodPost = @"POST";
static NSString* const kHttpMethodGet = @"GET";

// For loading view
#define kTagTransView  7777


// Indicate in debugging mode
#ifdef DEBUG
    #define IS_SANDBOX_MODE 1
#else
    #define IS_SANDBOX_MODE 0
#endif



