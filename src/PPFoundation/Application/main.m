//
//  main.m
//  PPFoundation
//
//  Created by Hieu Bui on 3/4/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PPAppDelegate.h"

int main(int argc, char *argv[]) {
	@autoreleasepool {
		return UIApplicationMain(argc, argv, nil, NSStringFromClass([PPAppDelegate class]));
	}
}
