//
//  AppDelegate.h
//  PPFoundation
//
//  Created by Hieu Bui on 3/4/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *nav;

@end
