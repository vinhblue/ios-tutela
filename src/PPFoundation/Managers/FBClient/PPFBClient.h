//
//  PPFBClient.h
//  ReengPlus
//
//  Created by Mai Huy Dong on 4/6/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Facebook.h>

@interface PPFBClient : NSObject
@property (strong, nonatomic) FBSession *session;

+ (instancetype)fbClient;

#pragma mark Users
- (void)loginCompletion:(void (^) (NSString *fbToken, NSError* error))completion;
- (void)getMeProfileCompletion:(void (^) (NSError* error, NSDictionary<FBGraphUser> *user))completion;
- (void)shareAnObject:(id)object completion:(void (^) (NSError* error))completion;
- (void)logout;

#pragma Friends
- (void)getFriendsListCompletion:(void (^) (NSArray* friends, NSError* error))completion;

@end
