//
//  PPFBClient.m
//  ReengPlus
//
//  Created by Mai Huy Dong on 4/6/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPFBClient.h"
#import "FBGraphUser.h"
#import <FBSessionTokenCachingStrategy.h>


@implementation PPFBClient


+ (instancetype)fbClient {
	DEFINE_SHARED_INSTANCE_USING_BLOCK ( ^{
	    return [[PPFBClient alloc] init];
	});
}
#pragma mark - Users

#pragma mark Login
- (void)loginCompletion:(void (^) (NSString *fbToken, NSError* error))completion {
	// Open facebook session, allow to login with UI
	[FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"user_birthday"] allowLoginUI:YES completionHandler: ^(FBSession *session, FBSessionState status, NSError *error) {
	    if (!error) {
	        DLog(@"%@", session.accessTokenData.accessToken);

	        // Nothing wrong and open success
	        if (status == FBSessionStateOpen) {
	            if (completion)
					completion(session.accessTokenData.accessToken, nil);
			}
        }else {
            // Internal server error
            completion(nil, error);
        }
        
        if(status == FBSessionStateClosedLoginFailed){
            [FBSession.activeSession closeAndClearTokenInformation];
            
            if (error) {
                // handle error here, for example by showing an alert to the user
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not login with Facebook"
                                                                message:@"Facebook login failed. Please check your Facebook settings on your phone."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }
        
	    
	}];
}

#pragma mark Logout
- (void)logout {
	// If session is open let's close and clear token information
	if ([FBSession activeSession].isOpen) {
		[[FBSession activeSession] closeAndClearTokenInformation];
	}
	else {
		// If session is currently close, but we had a cached token, let's re-open it then clear the token.
		if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded) {
			[[FBSession activeSession] openWithCompletionHandler: ^(FBSession *session, FBSessionState status, NSError *error) {
			    if (FB_ISSESSIONOPENWITHSTATE(status)) {
			        [[FBSession activeSession] closeAndClearTokenInformation];
				}
			    else {
			        DLog(@"Cound't re-open session with error: %@", error.userInfo);
				}
			}];
		}
	}
}

#pragma mark Share
- (void)requestPublishPermissionsCompletion:(void (^) (NSError* error))completion {
	// We need to request publish permission before sharing anything on user's facebook wall
	[FBSession.activeSession requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
	                                      defaultAudience:FBSessionDefaultAudienceFriends
	                                    completionHandler: ^(FBSession *session, NSError *error) {
        // No error
	    if (!error) {
            
            // The case user doens't allow this
	        if ([FBSession.activeSession.permissions
	             indexOfObject:@"publish_actions"] == NSNotFound) {
	            if (completion)
					completion([NSError errorWithDomain:@"facebook" code:200 userInfo:@{NSLocalizedDescriptionKey: @"Permission not granted"}]);
			}else {
            // User accepts publish request
	            if (completion)
					completion(nil);
			}
		}else {
	        // Internal server error
	        if (completion)
				completion(error);
		}
	}];
}
- (void)shareAnObject:(id)object completion:(void (^) (NSError* error))completion {
    
    // Before sharing on wall we need to make sure the user already granted publish permission
	[FBRequestConnection startWithGraphPath:@"/me/permissions"
	                      completionHandler: ^(FBRequestConnection *connection, id result, NSError *error) {
        
                              
	    if (!error) {
            
	        NSDictionary *permissions = [(NSArray *)[result data] objectAtIndex : 0];
	        if (![permissions objectForKey:@"publish_actions"]) {
	            // Publish permissions not found, ask for publish_actions
	            [self requestPublishPermissionsCompletion: ^(NSError* error) {
	                if (!error) {
	                    // Lets share here
                        
					}
	                else {
	                    if (completion)
	                        completion(error);
					}
				}];
			}else {
	            // Lets share here
			}
		}else {
	        // There was an error, handle it
	        if (completion)
	            completion([NSError errorWithDomain:@"facebook" code:200 userInfo:@{NSLocalizedDescriptionKey: @"Something went wrong, Please retry!"}]);
		}
	}];
}

#pragma mark Get profile
- (void)getMeProfileCompletion:(void (^) (NSError* error, NSDictionary <FBGraphUser> *user))completion {
    
    // Once user authorized, we can get his profile
	[FBRequestConnection startForMeWithCompletionHandler: ^(FBRequestConnection *connection, NSDictionary < FBGraphUser > *user, NSError *error) {
	    if (completion) {
	        completion(error, user);
		}
	}];
}
#pragma mark - Friends
- (void)getFriendsListCompletion:(void (^) (NSArray* friends, NSError* error))completion{
    
    [self loginCompletion:^(NSString *fbToken, NSError *error) {
        if (!error) {
            // Get friends list
            [FBRequestConnection startWithGraphPath:@"/me/friends?fields=id,name,picture" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, NSMutableDictionary* result, NSError *error) {
                if (!error) {
                    [result enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                        if ([key isEqualToString:@"data"]) {
                            if ([obj isKindOfClass:[NSArray class]]) {
                                
                                if(completion)
                                    completion(obj,nil);
                            }
                        }
                    }];
                }else{
                    if(completion)
                        completion(nil,error);
                }
            }];
        }
    }];
    
}
@end
