//
//  PPDataModel.m
//  PPFoundation
//
//  Created by Hieu Bui on 3/6/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import "PPDataManager.h"

@implementation PPDataManager

+ (instancetype)sharedManager {
	static PPDataManager *_sharedManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
	    _sharedManager = [PPDataManager new];
	});

	return _sharedManager;
}

- (id)init {
	self = [super init];
	if (self) {
	}
	return self;
}

- (void)saveLoggedUser:(NSDictionary *)userDict{
    // Model
    NSError* error = nil;
    UserModel* userM = [[UserModel alloc] initWithDictionary:userDict error:&error];
    if (error) {
        DLog(@"Unresolve error: %@",error.userInfo);
        return;
    }
    
    // Managed object
    [userM managerdObject];
    
    // Hold neccessary data
    PPSession* session = [PPSession sharedSession];
    session.isAuthenticated = YES;
    session.currentUserId = userM.user_id;
    [session save];
}
- (User *)currentLoogedUser{
    return [User MR_findFirstByAttribute:@"user_id" withValue:[PPSession sharedSession].currentUserId];
}

- (void) saveCauChuyen:(NSDictionary *)ccDict{
    // Model
    NSError* error = nil;
    CauChuyenModel* ccM = [[CauChuyenModel alloc] initWithDictionary:ccDict error:&error];
    if (error) {
        DLog(@"Unresolve error: %@",error.userInfo);
        return;
    }
    
    // Managed object
    [ccM managerdObject];

}

- (void) saveChienDich:(NSDictionary *)cdDict{
    // Model
    NSError* error = nil;
    ListCdModel* listCdM = [[ListCdModel alloc] initWithDictionary:cdDict error:&error];
    [listCdM.items enumerateObjectsUsingBlock:^(ChienDichModel* cdM, NSUInteger idx, BOOL *stop){
        if (error) {
            DLog(@"Unresolve error: %@",error.userInfo);
            *stop = YES;
        }
        [cdM managerdObject];
    }];
  
}

- (void) saveChienDichRed:(NSDictionary *)cdrDict{
    // Model
    NSError* error = nil;
    ListCDRModel * listcdrM = [[ListCDRModel alloc] initWithDictionary:cdrDict error:&error];
    [listcdrM.items enumerateObjectsUsingBlock:^(ChienDichRedModel* cdrM, NSUInteger idx, BOOL *stop){
        if (error) {
            DLog(@"Unresolve error: %@",error.userInfo);
            *stop = YES;
        }
        
        [cdrM managerdObject];
        
    }];

}
@end
