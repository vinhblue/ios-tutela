//
//  PPDataModel.h
//  PPFoundation
//
//  Created by Hieu Bui on 3/6/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface PPDataManager : NSObject

+ (instancetype)sharedManager;

- (void)saveLoggedUser:(NSDictionary *)userDict;

- (User *)currentLoogedUser;

- (void)saveCauChuyen:(NSDictionary *)ccDict;

- (void)saveChienDich:(NSDictionary *)cdDict;

- (void)saveChienDichRed:(NSDictionary *)cdrDict;

//- (void) saveStepOfChienDichRed:(NSDictionary *)
@end
