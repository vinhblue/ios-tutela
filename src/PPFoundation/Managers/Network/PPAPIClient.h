//
//  APIClient.h
//  PPFoundation
//
//  Created by Hieu Bui on 3/5/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@interface PPResponseObject : NSObject

@property (nonatomic, assign) PPStatusCode statusCode;
@property (nonatomic, strong) NSDictionary *responseBody;

- (void)printDescription;

+ (PPResponseObject *)responseObjectWithStatusCode:(NSInteger)statusCode responseObject:(id)responseObject;
+ (PPResponseObject *)responseObjectWithRequestOperation:(AFHTTPRequestOperation *)operation responseObject:(id)responseObject error:(NSError *)error;
@end

@interface PPAPIClient : AFHTTPRequestOperationManager

+ (instancetype)sharedClient;

#pragma mark -
#pragma mark Users
#pragma mark -

- (void)loginBySocial:(NSString *)socialId
                email:(NSString *)email
            firstName:(NSString *)firstName
             lastName:(NSString *)lastName
              address:(NSString *)address
                birth:(int) birth
           completion:(void (^) (PPResponseObject *responseObject))completion;

- (void) getCauChuyen:(int) user_id
        andFaceBookID:(double) fb_id
           completion:(void (^) (PPResponseObject *responseObject))completion;

- (void) getListChienDich:(int) user_id
            andFaceBookID:(double) fb_id
               completion:(void (^) (PPResponseObject *responseObject))completion;

- (void) getListChienDichDaDangKi:(int) user_id
                    andFaceBookID:(double) fb_id
                       completion:(void (^) (PPResponseObject *responseObject))completion;

- (void) dangkiChienDich:(int) user_id
           andFaceBookID:(double) fb_id
             idChienDich:(int) cd_id
              completion:(void (^) (PPResponseObject *responseObject))completion;

- (void) uploadFile:(int) user_id
           andFaceBookID:(double) fb_id
              cd_id:(int)ch_id
             picture:(UIImage *)picture
            comment:(NSString *)comment
              completion:(void (^) (PPResponseObject *responseObject))completion;
@end
