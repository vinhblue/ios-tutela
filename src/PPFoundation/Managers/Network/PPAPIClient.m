//
//  APIClient.m
//  PPFoundation
//
//  Created by Hieu Bui on 3/5/14.
//  Copyright (c) 2014 PIPU, Inc. All rights reserved.
//

#import "PPAPIClient.h"

@implementation PPResponseObject

- (id)init {
	self = [super init];
	if (self) {
		_statusCode = PPStatusCodeUndefined;
		_responseBody = nil;
	}
	return self;
}

- (void)printDescription {
	DLog(@"\nSTATUS CODE: %d\nRESPONSE BODY:\n%@", _statusCode, _responseBody);
}

+ (PPResponseObject *)responseObjectWithStatusCode:(NSInteger)statusCode responseObject:(id)responseObject {
	PPResponseObject *obj = [PPResponseObject new];
	obj.statusCode = (PPStatusCode)statusCode;
	obj.responseBody = responseObject;
	return obj;
}

+ (PPResponseObject *)responseObjectWithRequestOperation:(AFHTTPRequestOperation *)operation responseObject:(id)responseObject error:(NSError *)error{
    
    if (error) {
        DLog(@"ERROR:%@",error.userInfo);
        NSString *responseString = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        DLog(@"Server response string: %@", responseString);
    }else{
        PPResponseObject *obj = [PPResponseObject new];
        obj.statusCode = (PPStatusCode)operation.response.statusCode;
        obj.responseBody = responseObject;
        return obj;
    }
    return nil;
}
@end

@implementation PPAPIClient

+ (instancetype)sharedClient {
	static PPAPIClient *_sharedClient = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
	    _sharedClient = [[PPAPIClient alloc] initWithBaseURL:[NSURL URLWithString:kAPIBaseURLString]];
	});

	return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
	self = [super initWithBaseURL:url];
	if (self) {
		self.requestSerializer = [AFJSONRequestSerializer serializer];
		self.responseSerializer = [AFJSONResponseSerializer serializer];
	}
	return self;
}

#pragma mark - Base params
- (NSMutableURLRequest *)requestWithMethod:(NSString *)method parameters:(NSDictionary *)parameters path:(NSString *)path{
    NSMutableURLRequest* request = [self.requestSerializer requestWithMethod:@"GET" URLString:[[NSURL URLWithString:path relativeToURL:self.baseURL] absoluteString] parameters:parameters error:nil];
    
    if ([PPSession sharedSession].isAuthenticated) {
        [request setValue:[PPSession sharedSession].authToken forHTTPHeaderField:@"auth_token"];
    }
    
    return request;
}

#pragma mark - Request
- (void)request:(NSMutableURLRequest *)request completionBlock:(void (^) (PPResponseObject *responseObject))completion{
    
    [self HTTPRequestOperationWithRequest:request success: ^(AFHTTPRequestOperation *operation, id responseObject) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:responseObject error:nil];
        if (completion) {
            completion(responseObj);
        }
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:nil error:error];
        if (completion) {
            completion(responseObj);
        }
	}];
}


#pragma mark -
#pragma mark Users
#pragma mark -

- (void) loginBySocial:(NSString *)socialId
                 email:(NSString *)email
             firstName:(NSString *)firstName
              lastName:(NSString *)lastName
               address:(NSString *)address
                 birth:(int) birth
            completion:(void (^)(PPResponseObject *))completion{
        NSArray* keys = @[@"fb_id",@"email",@"first_name",@"last_name",@"city",@"birthday"];
    
        NSArray* objects = @[socialId,email,lastName,firstName,address,@(birth)];
    
    
    	NSString *path = @"login";
    	NSDictionary *parameters = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        DLog(@"%@",parameters);
    
    [self GET:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:responseObject error:nil];
        if (completion) {
            completion(responseObj);
        }
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:nil error:error];
        if (completion) {
            completion(responseObj);
        }
	}];
}

- (void) getCauChuyen:(int) user_id
        andFaceBookID:(double) fb_id
           completion:(void (^) (PPResponseObject *responseObject))completion{
    NSArray* keys = @[@"user_id",@"fb_id"];
    
    NSArray* objects = @[@(user_id),@(fb_id)];
    
    
    NSString *path = @"getstory";
    NSDictionary *parameters = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    DLog(@"%@",parameters);
    
    [self GET:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:responseObject error:nil];
        if (completion) {
            completion(responseObj);
        }
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:nil error:error];
        if (completion) {
            completion(responseObj);
        }
	}];

    
}

- (void) getListChienDich:(int) user_id
            andFaceBookID:(double) fb_id
               completion:(void (^) (PPResponseObject *responseObject))completion{
    NSArray* keys = @[@"user_id",@"fb_id"];
    
    NSArray* objects = @[@(user_id),@(fb_id)];
    
    
    NSString *path = @"getcd";
    NSDictionary *parameters = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    DLog(@"%@",parameters);
    
    [self GET:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:responseObject error:nil];
        if (completion) {
            completion(responseObj);
        }
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:nil error:error];
        if (completion) {
            completion(responseObj);
        }
	}];


}

- (void) getListChienDichDaDangKi:(int) user_id
                    andFaceBookID:(double) fb_id
                       completion:(void (^) (PPResponseObject *responseObject))completion{
    NSArray* keys = @[@"user_id",@"fb_id"];
    
    NSArray* objects = @[@(user_id),@(fb_id)];
    
    
    NSString *path = @"getcddk";
    NSDictionary *parameters = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    DLog(@"%@",parameters);
    
    [self GET:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:responseObject error:nil];
        if (completion) {
            completion(responseObj);
        }
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:nil error:error];
        if (completion) {
            completion(responseObj);
        }
	}];

    
}

- (void) dangkiChienDich:(int) user_id
           andFaceBookID:(double) fb_id
             idChienDich:(int)cd_id
              completion:(void (^) (PPResponseObject *responseObject))completion{
    NSArray* keys = @[@"user_id",@"fb_id",@"id_chiendich"];
    
    NSArray* objects = @[@(user_id),@(fb_id),@(cd_id)];
    
    
    NSString *path = @"dkcd";
    NSDictionary *parameters = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    DLog(@"%@",parameters);
    
    [self GET:path parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:responseObject error:nil];
        if (completion) {
            completion(responseObj);
        }
	} failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:nil error:error];
        if (completion) {
            completion(responseObj);
        }
	}];

}

- (void) uploadFile:(int)user_id
      andFaceBookID:(double)fb_id
              cd_id:(int)ch_id
            picture:(UIImage *)picture
            comment:(NSString *)comment
         completion:(void (^)(PPResponseObject *))completion{
    NSArray* keys = @[@"user_id",@"fb_id",@"id_chiendich",@"story"];
    
    NSArray* objects = @[@(user_id),@(fb_id),@(ch_id),comment];
    
    
    NSString *path = @"http://api.tutela.vn/site/uploadfile";
    NSDictionary *parameters = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    NSString* method = kHttpMethodPost;
    DLog(@"%@",parameters);
    
    
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:method URLString:path parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (picture) {
            [formData appendPartWithFileData:UIImagePNGRepresentation(picture) name:@"image" fileName:@"image.png" mimeType:@"image/png"];
        };
    } error:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:responseObject error:nil];
        if (completion) {
            completion(responseObj);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        PPResponseObject* responseObj = [PPResponseObject responseObjectWithRequestOperation:operation responseObject:nil error:error];
        if (completion) {
            completion(responseObj);
        }
    }];
    [[NSOperationQueue mainQueue] addOperations:@[operation] waitUntilFinished:NO];
}
@end
