//
//  PPUtility.m
//  ReengPlus
//
//  Created by Mai Huy Dong on 4/8/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import "PPUtility.h"

@implementation PPUtility

+ (instancetype)sharedUtility{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [[PPUtility alloc] init];
    });
}
#pragma mark App delegate
+ (PPAppDelegate *)appDelegate{
    PPAppDelegate *appDelegate = (PPAppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate;
}

#pragma mark Alert functions
- (void)showMessage:(NSString *)message withTitle:(NSString *)title {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    });
}


#pragma mark Loading View
- (void)showLoadingView{
    [[PPUtility sharedUtility] showLoadingViewInView:[PPUtility appDelegate].window];
}
- (void)hideLoadingView{
    [[PPUtility sharedUtility] hideLoadingViewInView:[PPUtility appDelegate].window];
}
- (void)showLoadingViewInView:(UIView*)view
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    if ([view viewWithTag:kTagTransView]) {
        UIView *transView = [view viewWithTag:kTagTransView];
        
        [UIView animateWithDuration:0.3 animations:^{
            transView.alpha = 0.6;
        }];
        
    }else{
        UIView *transView = [[UIView alloc] initWithFrame:view.bounds];
        transView.tag = kTagTransView;
        transView.backgroundColor = [UIColor grayColor];
        transView.alpha = 0.0;
        
        [view addSubview:transView];
        
        [UIView animateWithDuration:0.3 animations:^{
            transView.alpha = 0.6;
        }];
    }
    
    UIActivityIndicatorView* _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicatorView.center = view.center;
    [view addSubview:_activityIndicatorView];
    _activityIndicatorView.hidesWhenStopped = NO;
    _activityIndicatorView.tag = 7749;
    
    [_activityIndicatorView startAnimating];
    
    
    [UIView commitAnimations];
}

+ (UITableViewCell*)getCell:(Class)fromClass owner:(id) owner{
    NSArray *nib = nil;
    if(IS_IPAD){
        nib = [[NSBundle mainBundle] loadNibNamed:[NSStringFromClass(fromClass) stringByAppendingString:kiPadXibPostFix]
                                            owner:owner options:nil];
    }
    else{
        nib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(fromClass)
                                            owner:owner options:nil];
    }
    for (id oneObject in nib)
        if ([oneObject isKindOfClass:fromClass]){
            return (UITableViewCell *)oneObject;
        }
    return nil;
}


- (void)hideLoadingViewInView:(UIView*)view
{
    [UIView animateWithDuration:0.3 animations:^{
        
        UIView *transView = ( UIView *)[view viewWithTag:kTagTransView];
        transView.alpha = 0.0f;
        
        UIActivityIndicatorView* _activityIndicatorView = (UIActivityIndicatorView *)[view viewWithTag:7749];
        [_activityIndicatorView stopAnimating];
        [_activityIndicatorView removeFromSuperview];
        
    }];
}

- (void) animateTextField:(UITextField *)textField up:(BOOL)up distance:(int)distance view:(UIView *)view{
    int movementDistance = distance;
    
	const float movementDuration = 0.2f;
    
	int movement = (up ? -movementDistance : movementDistance);
    
	[UIView beginAnimations:@"anim" context:nil];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[UIView setAnimationDuration:movementDuration];
	view.frame = CGRectOffset(view.frame, 0, movement);
	[UIView commitAnimations];
}

@end
