//
//  PPUtility.h
//  ReengPlus
//
//  Created by Mai Huy Dong on 4/8/14.
//  Copyright (c) 2014 ROXWIN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPUtility : NSObject

+ (instancetype)sharedUtility;


#pragma mark App delegate
+ (PPAppDelegate *)appDelegate;

#pragma mark Alert functions
- (void)showMessage:(NSString *)message withTitle:(NSString *)title;

#pragma mark Loading view window style
- (void)showLoadingView;
- (void)hideLoadingView;
- (void)showLoadingViewInView:(UIView*)view;
- (void)hideLoadingViewInView:(UIView*)view;

+ (UITableViewCell*)getCell:(Class)fromClass owner:(id) owner;
- (void) animateTextField:(UITextField *)textField up:(BOOL)up distance:(int)distance view:(UIView *)view;
@end
