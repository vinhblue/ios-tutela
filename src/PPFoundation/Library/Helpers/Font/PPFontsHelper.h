//
//  PPFontsHelper.h
//  PPFoundation
//
//  Created by Hieu Bui on 12/29/12.
//  Copyright (c) 2012 PIPU, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPFontsHelper : NSObject

+ (void)printAllSystemFonts;

+ (UIFont *)elegantFontWithSize:(CGFloat)size;
+ (UIFont *)UTMFontWithSize:(CGFloat)size;
+ (UIFont *)UVFFontWithSize:(CGFloat)size;

@end
