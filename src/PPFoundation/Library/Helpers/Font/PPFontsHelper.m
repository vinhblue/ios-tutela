//
//  PPFontsHelper.m
//  PPFoundation
//
//  Created by Hieu Bui on 12/29/12.
//  Copyright (c) 2012 PIPU, Inc. All rights reserved.
//

#import "PPFontsHelper.h"

@implementation PPFontsHelper

+ (void)printAllSystemFonts {
	NSArray *familyNames = [UIFont familyNames];
	for (NSString *familyName in familyNames) {
		printf("Family: %s \n", [familyName UTF8String]);
		NSArray *fontNames = [UIFont fontNamesForFamilyName:familyName];
		for (NSString *fontName in fontNames) {
			printf("\tFont: %s \n", [fontName UTF8String]);
		}
	}
}

+ (UIFont *)elegantFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"Elegant Ink" size:size];
}
+ (UIFont *)UTMFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"UTM Cookies" size:size];
}
+ (UIFont *)UVFFontWithSize:(CGFloat)size{
    return [UIFont fontWithName:@"UVFQuickpen" size:size];
}

@end
